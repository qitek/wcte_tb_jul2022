//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jul 14 20:23:26 2022 by ROOT version 6.24/06
// from TTree midas_data1/First Digitizer
// found on file: root_run_000089_0000.root
//////////////////////////////////////////////////////////

#ifndef midasClass_h
#define midasClass_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TH1.h"

class midasClass {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           midasEvent;
   Int_t           timestamp;
   Int_t           serialnumber;
   Int_t           freqsetting;
   ULong_t         triggertime;
   TH1D            *Channel0;
   TH1D            *Channel1;
   TH1D            *Channel2;
   TH1D            *Channel3;
   TH1D            *Channel4;
   TH1D            *Channel5;
   TH1D            *Channel6;
   TH1D            *Channel7;

   // List of branches
   TBranch        *b_midasEvent;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_serialnumber;   //!
   TBranch        *b_freqsetting;   //!
   TBranch        *b_triggerTime;   //!
   TBranch        *b_Channel0;   //!
   TBranch        *b_Channel1;   //!
   TBranch        *b_Channel2;   //!
   TBranch        *b_Channel3;   //!
   TBranch        *b_Channel4;   //!
   TBranch        *b_Channel5;   //!
   TBranch        *b_Channel6;   //!
   TBranch        *b_Channel7;   //!

   midasClass(TFile *infile, TString treename);
   virtual ~midasClass();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif
