//////////////////////////////////////////////////////////

#ifndef midas_h
#define midas_h

#include <iostream>
#include <map>

#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"

#include "TH2.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TGraph.h"

#include "midasClass.h"

using namespace std;

// some nasty constants;)

// number of channels read out at the Test Beam at T9 line at CERN in July 2022
// 15 PMTs, channels 7 and 11 read out the same physical PMT
const int nChannels = 16;

/* the PMT map is https://docs.google.com/spreadsheets/d/1scoCYNb7RodWaWawr3xai6wEr4tchfL2M68FREvPNk4/edit?usp=sharing
Type	Detector	Channel	Patch Panel	Digitizer	Channel	Offline ChID	Index	Amplification
Signal	TOF0	0	B09	Regina (midas_data2)	0	8		x10
Signal	TOF0	1	B10	Regina	1	9		x10
Signal	TOF0	2	B19	Regina	2	10		x10
Signal	TOF0	3	B20	Regina	3	11		x10
Signal	ACT e	0	B08	TRIUMF (midas_data1)	0	0	01.06	x10
Signal	ACT e	1	B18	TRIUMF	1	1	01.06	x10
Signal	ACT mu	0	B07	TRIUMF	2	2	1.1	x1
Signal	ACT mu	1	B17	TRIUMF	3	3	1.1	x1
Signal	ACT tau	0	B06	TRIUMF	4	4	1.15	x1
Signal	ACT tau	1	B16	TRIUMF	5	5	1.15	x1
Signal	TOF1	0	B04	Regina	4	12		x10
Signal	TOF1	1	B14	Regina	5	13		x10
Signal	TOF1	2	B05	Regina	6	14		x10
Signal	TOF1	3	B15	Regina	7	15		x10
Signal	LGC	0	B03	TRIUMF	6	6		x1
Signal	TOF0	3	Duplicate(B20)	TRIUMF	7	7		x10
 */

// number of Aerogel Cherenkov Counters, aka ACTs;)
// the ACTs are called 0, 1, 2 or e, mu, tau based on their index of refraction
const int nActs = 3;

// for integrating the waveforms in fime
const int initialBinForCharge = 1;

// for histogramming
const int kMaxCuts = 50;

#include "TH1.h"

class midas {
public :

  midas(TString infilename, TString outfilename);
  void Loop(int argc, char *argv[], int debug);
  
  midasClass *m_midasClass1;
  midasClass *m_midasClass2;

  TString  m_outfilename;
  TString  m_infilename;

  TFile *m_infile;
  TFile *m_outfile;

  
private:

  int m_nCuts;
  TString m_cutnames[kMaxCuts];
  
  int nbins;
  double chtmax;
  double chtmin;
  
  double tw;
  double tmax;
  double tmin;
  double t0;
  double dtmax;
  double dtmin;

  int ntofbins;
  double tofmin;
  double tofmax;

  double minch;
  double maxch;

  double maxA;
  double minA;

  // for diffs
  double dchmin;
  double dchmax;

  // per channel
  TH1D* m_h_ampl[nChannels];
  TH1D* m_h_charge[nChannels];
  TH1D* m_h_CFD_time[nChannels];
  TH1D* m_h_global_time[nChannels];

  TString m_actStr[nActs] = {"ACTe", "ACTmu", "ACTtau"};
  TString m_actTitle[nActs] = {"ACT n=1.06", "ACT n=1.10", "ACT n=1.15"};

  
  // histograms and graphs
  std::map<TString,TH1D*> m_map_h1;
  std::map<TString,TH2D*> m_map_h2;
  std::map<TString,TGraph*> m_map_gr;

  void PrintMap1();
  void PrintMap2();
  void PrintMapGr();
  
  // utilities
  double getAmplitude(TH1D *h, double baseline, int imin);
  double getBaseline(TH1D *waveform, int baseline_samples = 20);
  int global_timing(TH1D* waveform, const int number_samples);
  double CFD_timing(TH1D* waveform, double baseline, const int global_imin, const float startp, const float endp, const float percentage, double & CFD_amplitude);
  double getCharge(TH1D* waveform, double baseline, const int A, const int B);

  double computeSum(double *darray, int i1, int i2);    
  double computeAverage(double *darray, int i1, int i2);
  double computeACTaverageCharge(double *charges, int i1, int i2);
  double computeACTSumAmpl(double *ampl, int i1, int i2);
  double computeTOF(double *CFD_time);
  double amplification(int ich);

  void MakeChannelHistos();
  
  TString getStr(int i, int j);
  void makeCorrHistos(int i1, int i2);
  void makeDiffHistos(int i1, int i2);
  void makeSumHistos(int i1, int i2);
  void MakeACTHistos();

  void FillCorrHistos(int i1, int i2, double *amplitude, double *global_time, double *CFD_time, double *charge);
  void FillDiffHistos(int i1, int i2, Long64_t jentry, double *amplitude, double *global_time, double *CFD_time, double *charge);
  void FillSumHistos(int i1, int i2, double *amplitude, double *global_time, double *CFD_time, double *charge);
  void FillACTTOFHistos(TString cutname, double t_TOF, double *ACTAverCharge, double *ACTSumAmpl);
  
};


#endif
