#!/usr/bin/python3

#from __future__ import print_function

import ROOT
from math import sqrt, pow, log, exp
import os, sys, getopt

from runs_momenta import *

cans = []
stuff = []

##########################################
# https://www.tutorialspoint.com/python/python_command_line_arguments.htm
def main(argv):
    #if len(sys.argv) > 1:
    #  foo = sys.argv[1]

    ### https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    ### https://pymotw.com/2/getopt/
    ### https://docs.python.org/3.1/library/getopt.html
    gBatch = False
    gTag=''
    print(argv[1:])
    try:
        # options that require an argument should be followed by a colon (:).
        opts, args = getopt.getopt(argv[2:], 'hbt:', ['help','batch','tag='])

        print('Got options:')
        print(opts)
        print(args)
    except getopt.GetoptError:
        print('Parsing...')
        print ('Command line argument error!')
        print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]]'.format(argv[0]))
        sys.exit(2)
    for opt,arg in opts:
        print('Processing command line option {} {}'.format(opt,arg))
        if opt == '-h':
            print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]'.format(argv[0]))
            sys.exit()
        elif opt in ("-b", "--batch"):
            gBatch = True
        elif opt in ("-t", "--tag"):
            gTag = arg
            print('OK, using user-defined histograms tag for output pngs {:}'.format(gTag,) )

    if gBatch:
        ROOT.gROOT.SetBatch(1)

    if len(argv) < 2:
        print('Usage: {} momentum'.format(argv[0]))
        print('   where momentum is e.g. 240p, 380n etc.')
        exit(1)
    reqMomentum = argv[1]

    reallyRun = True
    if len(argv) > 2:
        print(argv[2])
        if argv[2] == '0' or argv[2] == 'false' or argv[2] == 'False' or argv[2] == 'no' or argv[2] == 'No' or argv[2] == 'n' or argv[2] == 'N':
            reallyRun = False
    
    print('*** Settings:')
    print('tag={:}, batch={:}'.format(gTag, gBatch))

    runs = []
    for irun in runsMomentaDict:
        if reqMomentum in runsMomentaDict[irun]:
            runs.append(irun)
    print(runs)

    # run ;-)
    cmds = []
    outs = ''
    print('*** Running...')
    for irun in runs:
        outf = 'histos_run_000{}_0000_clean.root'.format(irun)
        cmd = './bin/AnalyzeTB_x data/root_run_000{}_0000_clean.root {}'.format(irun, outf)
        cmds.append(cmd)
        outs = outs + ' ' + outf
    for cmd in cmds:
        print(cmd)
        if reallyRun:
            os.system(cmd)
        else:
            print('NOT running on demand!')

            
    # merge;-)
    print('*** Merging...')
    mergedf = 'histos_runs_{}.root'.format(reqMomentum)
    cmd = 'hadd -f {} {}'.format(mergedf, outs)
    print(cmd)
    os.system(cmd)

    print('Consider:')

    print('python ./python/plotHistos.py {}'.format(mergedf))
    
    return

###################################
###################################
###################################

if __name__ == "__main__":
    # execute only if run as a script"
    main(sys.argv)
    
###################################
###################################
###################################

