#!/usr/bin/python3

#from __future__ import print_function

import ROOT
from math import sqrt, pow, log, exp
import os, sys, getopt

from runs_momenta import *

cans = []
stuff = []

##########################################
# https://www.tutorialspoint.com/python/python_command_line_arguments.htm
def main(argv):
    #if len(sys.argv) > 1:
    #  foo = sys.argv[1]

    ### https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    ### https://pymotw.com/2/getopt/
    ### https://docs.python.org/3.1/library/getopt.html
    gBatch = False
    gTag=''
    print(argv[1:])
    try:
        # options that require an argument should be followed by a colon (:).
        opts, args = getopt.getopt(argv[2:], 'hbt:', ['help','batch','tag='])

        print('Got options:')
        print(opts)
        print(args)
    except getopt.GetoptError:
        print('Parsing...')
        print ('Command line argument error!')
        print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]]'.format(argv[0]))
        sys.exit(2)
    for opt,arg in opts:
        print('Processing command line option {} {}'.format(opt,arg))
        if opt == '-h':
            print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]'.format(argv[0]))
            sys.exit()
        elif opt in ("-b", "--batch"):
            gBatch = True
        elif opt in ("-t", "--tag"):
            gTag = arg
            print('OK, using user-defined histograms tag for output pngs {:}'.format(gTag,) )

    if gBatch:
        ROOT.gROOT.SetBatch(1)

    if len(argv) < 1:
        print('Usage: {} [reallyrun=0/1]'.format(argv[0]))
        exit(1)

    reallyRun = False
    if len(argv) > 1:
        print(argv[1])
        if argv[1] == '0' or argv[1] == 'false' or argv[1] == 'False' or argv[1] == 'no' or argv[1] == 'No' or argv[1] == 'n' or argv[1] == 'N':
            reallyRun = False
        if argv[1] == '1' or argv[1] == 'true' or argv[1] == 'True' or argv[1] == 'yes' or argv[1] == 'Yes' or argv[1] == 'y' or argv[1] == 'Y':
            reallyRun = True

    reallyPlot = False
    if len(argv) > 2:
        print(argv[1])
        if argv[2] == '0' or argv[2] == 'false' or argv[2] == 'False' or argv[2] == 'no' or argv[2] == 'No' or argv[2] == 'n' or argv[2] == 'N':
            reallyPlot = False
        if argv[2] == '1' or argv[2] == 'true' or argv[2] == 'True' or argv[2] == 'yes' or argv[2] == 'Yes' or argv[2] == 'y' or argv[2] == 'Y':
            reallyPlot = True


            
    print('*** Settings:')
    print('tag={:}, batch={:}'.format(gTag, gBatch))


    momenta = []
    for irun in runsMomentaDict:
        momentum = runsMomentaDict[irun]
        if not momentum in momenta:
            momenta.append(momentum)

            
    cmds = []
    for momentum in momenta:
        cmd = 'python python/RunOverMomentum.py {}'.format(momentum)
        cmds.append(cmd)
    for cmd in cmds:
        print(cmd)
        if reallyRun:
            os.system(cmd)
        else:
            pass
            #print('NOT running on demand!')

    # plotting
    cmds = []
    for momentum in momenta:
        cmd = 'python python/plotHistos.py histos_runs_{}.root -b'.format(momentum)
        cmds.append(cmd)
    for cmd in cmds:
        print(cmd)
        if reallyPlot:
            os.system(cmd)
        else:
            pass
            #print('NOT plottig on demand!')

            
    return

###################################
###################################
###################################

if __name__ == "__main__":
    # execute only if run as a script"
    main(sys.argv)
    
###################################
###################################
###################################

