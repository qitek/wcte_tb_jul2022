#!/usr/bin/python3
# Thu 14.7. 2022 11:36:10 CEST

#from __future__ import print_function

import ROOT
from math import sqrt, pow, log, exp
import os, sys, getopt

from collections import OrderedDict

from tofCompute import GenerateTimes, momentaMeV
from runs_momenta import runsMomentaDict

cans = []
stuff = []

def TofFit(h1, ileg = 0, ioffx = 0.18):
    h1.SetTitle('')
    x1 = h1.GetXaxis().GetXmin()
    x2 = h1.GetXaxis().GetXmax()
    imax = h1.GetMaximumBin()
    bw = h1.GetBinWidth(imax)
    print('bw: {}'.format(bw))
    mean = h1.GetMean()
    a = h1.GetBinContent(imax)
    form1 = '[0]*exp(-(x-[1])^2/(2*[2]^2))'
    form = '[0]*exp(-(x-[1])^2/(2*[2]^2)) + [3]*exp(-(x-[4])^2/(2*[5]^2)) + [6]*exp(-(x-[7])^2/(2*[8]^2))'
    gfuns = []
    tlabels = ['e', '#mu', '#pi', 'all']
    labels = ['e', 'mu', 'pi', 'all']
    for i in range(0,3):
        fname = 'gfit_{}'.format(labels[i])
        fun = ROOT.TF1(fname, form1, x1, x2)
        gfuns.append(fun)
    fname = 'gfit_Full'.format(labels[i])
    Fun = ROOT.TF1(fname, form, x1, x2)
    gfuns.append(Fun)
    Fun.SetLineColor(h1.GetLineColor())
    fracs = [0.9, 0.05, 0.025]
    w = 0.5
    #Fun.SetParameters(a*fracs[0], mean*0.90, w,
    #                  a*fracs[1], mean*1.2, w,
    #                  a*fracs[2], mean*1.35, w)
    #Fun.SetParameters(a*fracs[0], mean-0.5, w,
    #                  a*fracs[1], mean+1, 2*w,
    #                  a*fracs[2], mean+2, 3*w)
    x0 = 63.5 #39.6
    #Fun.SetParameters(a*fracs[0], x0, 0.3,
    #                  a*fracs[1], x0+2, 2*w,
    #                  a*fracs[2], x0+2.5, 3*w)
    Fun.SetParameters(a*fracs[0], x0, 0.3,
                      a*fracs[1], 65.6, w,#40.5
                      a*fracs[2], 66.4,  w)#41.5
    h1.Fit(Fun, "0")
    for i in range(0,3):
        gfuns[i].SetParameters(Fun.GetParameter(0+3*i),
                               Fun.GetParameter(1+3*i),
                               Fun.GetParameter(2+3*i)
        )
    fitleg = ROOT.TLegend(0.16 + ileg*ioffx, 0.65, 0.16 + 0.18 + ileg*ioffx, 0.75)
    fitleg.SetBorderSize(0)
    totalI = Fun.Integral(x1,x2) / bw
    pfrac = []
    indices = [3, 0, 1, 2]
    for j in range(0, len(gfuns)):
        fun.SetNpx(1000)
        i = indices[j]
        fun = gfuns[i]
        fun.SetLineColor(h1.GetLineColor())
        fun.SetLineStyle(j+1)
        if indices[j] == 3:
            fun.SetLineStyle(1)
            fun.SetLineWidth(2)
        n = fun.Integral(x1,x2) / bw
        frac = 0.
        if totalI > 0:
            frac = n / (1.*totalI)
            NfitTag = 'N^{fit}'
            if fun == Fun:
                lentry = '{}: {}={:.0f}'.format(tlabels[i], NfitTag, n)
            else:
                lentry = '{}: {}={:.0f} f={:1.3f}'.format(tlabels[i], NfitTag, n, frac)
            fitleg.AddEntry(fun, lentry, 'L')
        pfrac.append(frac)
    pimufrac = 'NAN'
    if pfrac[2] > pfrac[3] and pfrac[2] > 0:
        pimufrac = '{:1.2f}'.format(pfrac[3]/pfrac[2])
    elif pfrac[3] > pfrac[2] and pfrac[3] > 0:
        pimufrac = '{:1.2f}'.format(pfrac[2]/pfrac[3])
    fitleg.SetHeader('N={:.0f}, #pi/#mu: {}'.format(h1.Integral(), pimufrac))
    return fitleg,gfuns

def SetOpt1d(h, col = ROOT.kBlack, fcol = ROOT.kMagenta):
    h.SetLineColor(col)
    h.SetMarkerColor(col)
    h.SetLineStyle(1)
    h.SetLineWidth(1)
    if fcol > 0:
        h.SetFillColor(fcol)
        h.SetFillStyle(1111)
    h.SetMarkerStyle(20)
    h.SetMarkerSize(1)
    #h.SetMinimum(0.)
    #h.SetStats(0)


def DrawTofLines(h, dtmu, dtpi):
    imax = h.GetMaximumBin()
    xe = h.GetBinCenter(imax)

    y0 = h.GetMinimum()
    y1 = 1.1*h.GetBinContent(imax)
    
    line_e = ROOT.TLine(xe, y0, xe, y1)
    line_e.SetLineColor(ROOT.kRed)

    line_mu = ROOT.TLine(xe+dtmu, y0, xe+dtmu, y1)
    line_mu.SetLineColor(ROOT.kBlue)

    line_pi = ROOT.TLine(xe+dtpi, y0, xe+dtpi, y1)
    line_pi.SetLineColor(ROOT.kBlack)

    lines = [line_e, line_mu, line_pi]
    for line in lines:
        line.SetLineStyle(2)
        line.Draw()
    return lines

##########################################
# https://www.tutorialspoint.com/python/python_command_line_arguments.htm
def main(argv):
    #if len(sys.argv) > 1:
    #  foo = sys.argv[1]

    ### https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    ### https://pymotw.com/2/getopt/
    ### https://docs.python.org/3.1/library/getopt.html
    gBatch = False


    try:
        # options that require an argument should be followed by a colon (:).
        opts, args = getopt.getopt(argv[2:], 'hbt:', ['help','batch','tag='])

        print('Got options:')
        print(opts)
        print(args)
    except getopt.GetoptError:
        print('Parsing...')
        print ('Command line argument error!')
        print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]]'.format(argv[0]))
        sys.exit(2)
    for opt,arg in opts:
        print('Processing command line option {} {}'.format(opt,arg))
        if opt == '-h':
            print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]'.format(argv[0]))
            sys.exit()
        elif opt in ("-b", "--batch"):
            gBatch = True
        elif opt in ("-t", "--tag"):
            gTag = arg
            print('OK, using user-defined histograms tag for output pngs {:}'.format(gTag,) )

    
    if gBatch:
        ROOT.gROOT.SetBatch(1)

    ROOT.gStyle.SetPalette(ROOT.kDarkBodyRadiator)
    ROOT.gStyle.SetPadLeftMargin(0.15)
    
    if len(argv) < 2:
        print('Usage:')
        print('{} histosFile.root'.format(argv[0]))
        return
    
    filename = argv[1]
    infile = ROOT.TFile(filename, 'read')

    ftag = filename.replace('.root','')
    runtag = ftag.split('_')[2]
    irun = runtag.replace('0000','').replace('000','')
    if len(ftag.split('_')) > 4:
        runtag = runtag + '_' + ftag.split('_')[4]

    outdir = 'plots_{}/'.format(runtag)
    os.system('mkdir -p {}'.format(outdir))
    
    varnames = { 'amplitude' : ROOT.kMagenta,
                 'charge' : ROOT.kCyan,
                 'global_time' : ROOT.kGray,
                 'CFD_time' : ROOT.kGreen,
    }
    
    nCh = 16
    hnames = {}
    for varname in varnames:
        hnames[varname] = []
        for ich in range(0, nCh):
            name = '{}_{}'.format(varname, ich)
            hnames[varname].append(infile.Get(name))
        
        canname = 'bPlots_{}_{}'.format(varname, runtag)
        can = ROOT.TCanvas(canname, canname)
        can.Divide(4,4)
        cans.append(can)
        ican = 0
        for hh in hnames[varname]:
            ican = ican + 1
            can.cd(ican)
            col = varnames[varname]
            SetOpt1d(hh, ROOT.kBlack, col)
            hh.Draw()
            hh.SetStats(0)
            if 'charge' in hh.GetName():
                ROOT.gPad.SetLogy()
        can.Update()

    ####################################################
    # special 2D
    h2ampl_7_11 = infile.Get('h2_corr_amplitude_7_11')
    h2time_7_11 = infile.Get('h2_corr_CFD_time_7_11')
    h2ampl_8_9 = infile.Get('h2_corr_amplitude_8_9')
    h2time_8_9 = infile.Get('h2_corr_CFD_time_8_9')
    h2ampl_0_1 = infile.Get('h2_corr_amplitude_0_1')
    h2time_0_1 = infile.Get('h2_corr_CFD_time_0_1')
    h2s = [h2ampl_7_11, h2time_7_11,  h2ampl_8_9,  h2time_8_9,  h2ampl_0_1,  h2time_0_1 ]
    corrs = {}
    for h2 in h2s:
        h2.SetStats(0)
        rho = h2.GetCorrelationFactor()
        txt = ROOT.TLatex(0.13, 0.92, '#rho=' + '{:1.2f}'.format(rho))
        txt.SetTextColor(ROOT.kRed)
        txt.SetNDC()
        corrs[h2] = txt
    canname = 'Plots2d_{}_{}'.format(varname, runtag)
    can2d = ROOT.TCanvas(canname, canname, 0, 0, 1000, 1000)
    can2d.Divide(2,3)
    ican = 1
    for h2 in h2s:
        can2d.cd(ican)
        h2.Draw('colz')
        corrs[h2].Draw()
        ican = ican + 1
    stuff.append(h2s)
    stuff.append(corrs)
    cans.append(can2d)

    ####################################################
    # special diff histos
    hdiffampl_7_11 = infile.Get('h1_diff_amplitude_7_11')
    hdifftime_7_11 = infile.Get('h1_diff_CFD_time_7_11')
    hdiffcharge_7_11 = infile.Get('h1_diff_charge_7_11')
    hdiffampl_8_9 = infile.Get('h1_diff_amplitude_8_9')
    hdifftime_8_9 = infile.Get('h1_diff_CFD_time_8_9')
    hdiffcharge_8_9 = infile.Get('h1_diff_charge_8_9')
    hdiffampl_0_1 = infile.Get('h1_diff_amplitude_0_1')
    hdifftime_0_1 = infile.Get('h1_diff_CFD_time_0_1')
    hdiffcharge_0_1 = infile.Get('h1_diff_charge_0_1')
    hdiffs = [hdiffampl_7_11, hdifftime_7_11, hdiffcharge_7_11,
              hdiffampl_8_9,  hdifftime_8_9,hdiffcharge_8_9,
              hdiffampl_0_1,  hdifftime_0_1, hdiffcharge_0_1,
    ]
    #corrs = {}
    for hdiff in hdiffs:
        for varname in varnames:
            if varname in hdiff.GetName():
                hdiff.SetFillColor(varnames[varname])
                hdiff.SetFillStyle(1111)
        #    hdiff.SetStats(0)
    canname = 'Diffs1d_{}_{}'.format(varname, runtag)
    candiff = ROOT.TCanvas(canname, canname, 0, 0, 1000, 1000)
    candiff.Divide(2,3)
    ican = 1
    for hdiff in hdiffs:
        candiff.cd(ican)
        hdiff.Draw('hist')
        ican = ican + 1
    stuff.append(hdiffs)
    stuff.append(corrs)
    cans.append(candiff)

    ####################################################
    # sum histos
    hsumampl_0_1 = infile.Get('h1_sum_amplitude_0_1')
    #hsumtime_0_1 = infile.Get('h1_sum_CFD_time_0_1')
    hsumcharge_0_1 = infile.Get('h1_sum_charge_0_1')
    hsums = [ hsumampl_0_1,
              #hsumtime_0_1,
              hsumcharge_0_1,  ]
    #corrs = {}
    for hsum in hsums:
        for varname in varnames:
            if varname in hsum.GetName():
                hsum.SetFillColor(varnames[varname])
                hsum.SetFillStyle(1111)
        #    hsum.SetStats(0)
    canname = 'Sums1d_{}_{}'.format(varname, runtag)
    cansum = ROOT.TCanvas(canname, canname, 0, 0, 1200, 600)
    cansum.Divide(2,1)
    ican = 1
    for hsum in hsums:
        cansum.cd(ican)
        hsum.Draw('hist')
        ican = ican + 1
    stuff.append(hsums)
    stuff.append(corrs)
    cans.append(cansum)

    ####################################################
    # amplitude diff stability:
    grnames = ['gr_diff_amplitude_0_1',
               'gr_diff_amplitude_7_11',
               'gr_diff_amplitude_8_9',
               ]
    canname = 'aGrs1d_Stability_{}'.format(runtag)
    cangr = ROOT.TCanvas(canname, canname, 0, 200, 1500, 500)
    cangr.Divide(3,1)
    ican = 1
    grcols = [ROOT.kBlue, ROOT.kRed, ROOT.kBlack]
    for grname in grnames:
        gr = infile.Get(grname)
        cangr.cd(ican)
        gr.SetMarkerColor(grcols[ican-1])
        gr.Draw('AP')
        ican = ican + 1
        stuff.append(gr)
    cans.append(cangr)
    cangr.Update()


    ####################################################
    
    cutsStr = OrderedDict()
    cutsStr['_noCuts']                                                  = 'no cuts'
    #cutsStr['_ACT0AverSumAmplCut']                                = '+ACT1.06 Aver Charge > -15000'
    cutsStr['_ACT0SumAmplCut']                                = '+ACT1.06 Sum Amplitute < 250.'
    cutsStr['_ACT0SumAmplCut_ACT1SumAmplCut']                 = '+ACT1.10 Sum Amplitute < 35.'
    cutsStr['_ACT0SumAmplCut_ACT1SumAmplCut_ACT2SumAmplCut']  = '+ACT1.15 Sum Amplitute < 60.'
    

    canname = 'Plots1dCuts'
    can1d = ROOT.TCanvas(canname, canname, 100, 100, 1600, 1000)
    can1d.Divide(3,2)
    cans.append(can1d)

    canname = 'tTOFCuts'
    cantof = ROOT.TCanvas(canname, canname, 400, 0, 1000, 1000)
    cans.append(cantof)

    tofTimesDiffs = GenerateTimes()
    momentum = 220
    momentumKnown = False
    print('irun: {}'.format(irun))

    if irun in runsMomentaDict:
        sign = 1
        if 'n' in runsMomentaDict[irun]:
            sign = -1
        momentum = int(runsMomentaDict[irun].replace('p','').replace('n','')) * sign
        print('Run: {} Momentum: {}'.format(irun, momentum))
        momentumKnown = True
    if not momentumKnown:
        if 'n' in irun or 'p' in irun:
            momentum = int(irun.replace('p','').replace('n',''))
            momentumKnown = True
   
    ipmu = momentaMeV.index(abs(momentum))
    dtmu = tofTimesDiffs['mu'][ipmu]
    ippi = momentaMeV.index(abs(momentum))
    dtpi = tofTimesDiffs['pi'][ippi]
    print('TOF shifts: mu: {} pi: {}, their diff: {}'.format(dtmu, dtpi, dtmu-dtpi))
    
    opt = ''
    cutcols = [ROOT.kRed, ROOT.kBlack, ROOT.kBlue, ROOT.kGreen+2, ]
    icut = -1
    leg = ROOT.TLegend(0.18, 0.76, 0.88, 0.89)
    leg.SetBorderSize(0)
    ptag = ''
    if momentumKnown:
        ptag = 'Run {}, p = {} MeV'.format(irun, abs(momentum))
        if momentum > 0:
            ptag += ' (Pos) '
        else:
            ptag += ' (Neg) '
    else:
        ptag = 'Run {}, p unknown'.format(irun)
    leg.SetHeader('{}'.format(ptag))
    stuff.append(leg)

    tofh1s = []
    for cutstr in cutsStr:
        
        icut = icut + 1

        canname = 'Plots2d' + cutstr
        can2d = ROOT.TCanvas(canname, canname, 0, 0, 800, 600)
        can2d.Divide(4,3)
        ican = 0
        h2names = ['ACTeAverCharge_vs_t_TOF' + cutstr,
                   'ACTmuAverCharge_vs_t_TOF' + cutstr,
                   'ACTtauAverCharge_vs_t_TOF' + cutstr,
                   'ACTeAverCharge_vs_ACTmuAverCharge' + cutstr,
                   'ACTeAverCharge_vs_ACTtauAverCharge' + cutstr,
                   'ACTmuAverCharge_vs_ACTtauAverCharge' + cutstr,
                   'ACTeSumAmpl_vs_t_TOF' + cutstr,
                   'ACTmuSumAmpl_vs_t_TOF' + cutstr,
                   'ACTtauSumAmpl_vs_t_TOF' + cutstr,
                   'ACTeSumAmpl_vs_ACTmuSumAmpl' + cutstr,
                   'ACTeSumAmpl_vs_ACTtauSumAmpl' + cutstr,
                   'ACTmuSumAmpl_vs_ACTtauSumAmpl' + cutstr,
        ]
        for h2name in h2names:
            h2 = infile.Get(h2name)
            ican = ican + 1
            stuff.append(h2)
            can2d.cd(ican)
            h2.SetStats(0)
            h2.Draw('colz')
            ROOT.gPad.SetLogz(1)
        cans.append(can2d)
        can2d.Update()
        
        ican = 0
        h1names = ['t_TOF' + cutstr,
                   'ACTeAverCharge' + cutstr,
                   'ACTmuAverCharge' + cutstr,
                   'ACTtauAverCharge' + cutstr,
                   'ACTeSumAmpl' + cutstr,
                   'ACTmuSumAmpl' + cutstr,
                   'ACTtauSumAmpl' + cutstr,
        ]

        for h1name in h1names:
            h1 = infile.Get(h1name)
            #print(h1name, h1)
            stuff.append(h1)
            h1.SetLineColor(cutcols[icut])
            h1.SetMaximum(1.90*h1.GetMaximum())
            if h1name == h1names[0]:
                cantof.cd()
                tofh1s.append(h1)
            else:
                ican = ican + 1
                can1d.cd(ican)
            h1.SetMarkerColor(h1.GetLineColor())
            h1.SetMarkerSize(1)
            h1.SetMarkerStyle(20)
            h1.SetStats(0)
            if h1name == h1names[0]:
                leg.AddEntry(h1, cutsStr[cutstr], 'L')
            if icut == len(cutsStr)-1:
                leg.Draw()
            if h1name == h1names[0]:
                h1.Draw("e1 hist x0" + opt)
                #if icut == len(cutsStr)-1:
                if icut == 3:
                    fitleg,funs = TofFit(h1, icut)
                    for fun in funs:
                        fun.Draw('same')
                    fitleg.Draw()
                    stuff.append(funs)
                    stuff.append(fitleg)
            else:
                h1.SetMaximum(10*h1.GetMaximum())
                h1.Draw("hist" + opt)
                ROOT.gPad.SetLogy(1)
                ROOT.gPad.Update()
                                
        can1d.Update()
        opt = "same"

    cantof.cd()
    print('dtmu: {:2.3f} dtpi: {:2.3f}'.format(dtmu, dtpi))
    lines = DrawTofLines(tofh1s[0], dtmu, dtpi)
    stuff.append(lines)
    
    for can in cans:
        can.Print(outdir + can.GetName() + '.png')
        can.Print(outdir + can.GetName() + '.pdf')

    

    os.system('cd {} ; ../scripts/CreateHTMLpage_noConv.sh {}'.format(outdir, runtag))
    print('firefox {}/index.html'.format(outdir))


    
    if not gBatch:
        ROOT.gApplication.Run()
    return

###################################
###################################
###################################

if __name__ == "__main__":
    # execute only if run as a script"
    main(sys.argv)
    
###################################
###################################
###################################

