#!/usr/bin/python3
# 12/07/2022 14:01:56 CEST


import ROOT
from math import sqrt, pow, log, exp
import os, sys, getopt

gcans = []
stuff = []


def DrawRho(h2):
    rho = h2.GetCorrelationFactor()
    txt= ROOT.TLatex(0.13, 0.83, '#rho=' + '{:1.2f}'.format(rho))
    txt.SetNDC()
    stuff.append(txt)
    txt.Draw()

def GetChannelTagAndTitle(ich):
    chtag = '_ch{}'.format(ich)
    chtit = 'Channel {} '.format(ich)
    if ich == -1:
        chtag = ''
        chtit = 'All channels '
    return chtag,chtit

def SetOpts(h, col = ROOT.kBlue):
    h.SetLineColor(col)
    h.SetMarkerColor(col)
    h.SetLineStyle(1)
    h.SetLineWidth(1)
    h.SetMarkerStyle(20)
    h.SetMarkerSize(1)
    h.SetMinimum(0.)
    h.SetStats(0)
    h.SetMinimum(3300.)
    h.SetMaximum(3650.)

    
##########################################
# https://www.tutorialspoint.com/python/python_command_line_arguments.htm
def main(argv):
    #if len(sys.argv) > 1:
    #  foo = sys.argv[1]

    ### https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    ### https://pymotw.com/2/getopt/
    ### https://docs.python.org/3.1/library/getopt.html
    gBatch = False
    gTag=''
    print(argv[1:])
    try:
        # options that require an argument should be followed by a colon (:).
        opts, args = getopt.getopt(argv[2:], 'hbt:', ['help','batch','tag='])

        print('Got options:')
        print(opts)
        print(args)
    except getopt.GetoptError:
        print('Parsing...')
        print ('Command line argument error!')
        print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]]'.format(argv[0]))
        sys.exit(2)
    for opt,arg in opts:
        print('Processing command line option {} {}'.format(opt,arg))
        if opt == '-h':
            print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]'.format(argv[0]))
            sys.exit()
        elif opt in ("-b", "--batch"):
            gBatch = True
        elif opt in ("-t", "--tag"):
            gTag = arg
            print('OK, using user-defined histograms tag for output pngs {:}'.format(gTag,) )

    if gBatch:
        ROOT.gROOT.SetBatch(1)

    print('*** Settings:')
    print('tag={:}, batch={:}'.format(gTag, gBatch))


    ############
    #   Read   #
    ############
    
    if len(argv) < 2:
        print('Usage: {} rootfile.root'.format(argv[0]))
        return
    filename = argv[1]
    rfile = ROOT.TFile(filename, 'read')
    treename = 'events'
    
    fchain = ROOT.TChain(treename)
    fchain.Add(filename)
    
    nEvts = fchain.GetEntries() 
    print('nEvts: {}'.format(nEvts))
    print(fchain)
    
    ############
    #   Loop   #
    ############

    nCh = 16
    
    nReq = nEvts
    # HACKS!
    nReq = 1000
    iDraw = 10
    
    canname = 'WaveForms1d'
    cw = 1000
    ch = 500
    #can = ROOT.TCnvas(canname, canname, 200, 200, cw, ch)
    #can.Divide(2,1)
    #gcans.append(can)

    hnames = {'CFD_time'    : [0., 256.],
              'global_time' : [0., 256.],
              'amplitude'   : [-1712, 0.],
              'charge' : [-35e3, 0.],
    }
    nbins = 100
    
    histos = {}

    # 1D
    for hname in hnames:
        x1 = hnames[hname][0]
        x2 = hnames[hname][1]
        histos[hname] = []
        for ich in range(0, nCh):
            name = 'h_{}_{}'.format(hname, ich)
            title = hname + ';' + hname
            histos[hname].append(ROOT.TH1D(name, title, nbins, x1, x2))

    # 2D between vars for all channels and within each channel;-)
    ih1 = -1
    histos2d = {}
    for ich in range(-1, nCh):
        chtag, chtit = GetChannelTagAndTitle(ich)
        for hname1 in hnames:
            ih1 = ih1 + 1
            ih2 = -1
            for hname2 in hnames:
                ih2 = ih2 + 1
                if ih2 >= ih1:
                    continue
                x1 = hnames[hname1][0]
                x2 = hnames[hname1][1]
                y1 = hnames[hname2][0]
                y2 = hnames[hname2][1]
                name = 'h2_{}_vs_{}{}'.format(hname2, hname1, chtag)
                title = '{}{} vs {};{};{}'.format(chtit, hname2, hname1, hname1, hname2)
                histos2d[name] = ROOT.TH2D(name, title, nbins, x1, x2, nbins, y1, y2)
                histos2d[name].SetStats(0)

    # 2D for each variable between channels:
    histosVar2d = {}
    for ich1 in range(0, nCh):
        chtag1,chtit1 = GetChannelTagAndTitle(ich1)
        for ich2 in range(0, nCh):
            if ich2 >= ich1:
                continue
            chtag2,chtit2 = GetChannelTagAndTitle(ich2)
            for hname in hnames:
                name = 'h2_{}{}_vs_{}{}'.format(hname, chtag1, hname, chtag2)
                title = '{}{} vs {}{};{}{};{}{}'.format(chtit2, hname, chtit1, hname, chtit1, hname, chtit2, hname)
                x1 = hnames[hname][0]
                x2 = hnames[hname][1]
                histosVar2d[name] = ROOT.TH2D(name, title, nbins, x1, x2, nbins, x1, x2)
                histosVar2d[name].SetStats(0)
                
            
    #################
    #     LOOP!     #
    #################
        
            
    for ievt in range(0, nReq):

        if ievt % iDraw == 0:
            print('{} / {} / {}'.format(ievt, nReq, nEvts), end = '\r')
        # get the next tree in the chain
        ientry = fchain.LoadTree(ievt)
        if ientry < 0:
            print('Breaking!')
            break

        # verify file/tree/chain integrity
        nb = fchain.GetEntry( ievt )
        if nb <= 0:
            print('Sorry, continuing...')
            continue

        
        #################
        # fill 1d
        #################

        nCh = fchain.nCh
        for ich in range(0, nCh):
            for hname in hnames:
                histos[hname][ich].Fill(eval('fchain.{}[{}]'.format(hname, ich)))

        ##################################
        # fill 2d within a channel
        ##################################
        ih1 = -1
        for hname1 in hnames:
            ih1 = ih1 + 1
            ih2 = -1
            for hname2 in hnames:
                ih2 = ih2 + 1
                if ih2 >= ih1:
                    continue
                for ich in range(0, nCh):
                    name = 'h2_{}_vs_{}_ch{}'.format(hname2, hname1, ich)
                    histos2d[name].Fill(eval('fchain.{}[{}]'.format(hname1,ich)), eval('fchain.{}[{}]'.format(hname2,ich)))
                    name = 'h2_{}_vs_{}'.format(hname2, hname1)
                    histos2d[name].Fill(eval('fchain.{}[{}]'.format(hname1,ich)), eval('fchain.{}[{}]'.format(hname2,ich)))

        
        ##################################
        # fill 2d between channels
        ##################################
        for ich1 in range(0, nCh):
            chtag1,chtit1 = GetChannelTagAndTitle(ich1)
            for ich2 in range(0, nCh):
                if ich2 >= ich1:
                    continue
                chtag2,chtit2 = GetChannelTagAndTitle(ich2)
                for hname in hnames:
                    name = 'h2_{}{}_vs_{}{}'.format(hname, chtag1, hname, chtag2)
                    histosVar2d[name].Fill(eval('fchain.{}[{}]'.format(hname,ich1)), eval('fchain.{}[{}]'.format(hname,ich2)))
 
    print('{} / {} / {}'.format(ievt, nReq, nEvts), end = '\r')
    print('Done loop!')
    
    ############
    #   PLOT   #
    ############

    cans = {}

    ROOT.gStyle.SetPalette(ROOT.kSolar)
    #ROOT.gStyle.SetOptTitle(0)
    
    ih = -1
    for hname in hnames:
        ih = ih+1
        canname = 'can_{}'.format(hname)
        cw = 800
        ch = 800
        print('Creating canvas {}'.format(canname))
        cans[hname] = ROOT.TCanvas(canname, canname, 100*ih, 100*ih, cw, ch)
        #cans[hname].Divide(2,2) # HARDCODED!
        opt = ''
        for ich in range(0, nCh):
            hh = histos[hname][ich]
            hh.SetStats(0)
            hh.Draw('PLC hist' + opt)
            opt = 'same'
            ROOT.gPad.SetLogy(1)
            ROOT.gPad.Update()
    stuff.append(histos)
    for cname in cans:
        gcans.append(cans[cname])
    
    # 2D, all channels and per channel
    cw = 1600
    ch = 800
    for ich in range(-1, nCh):
        chtag,chtit = GetChannelTagAndTitle(ich)
        canname = 'vars2d{}'.format(chtag)
        can2d = ROOT.TCanvas(canname, canname, 0, 0, cw, ch)
        can2d.Divide(3,2)
        gcans.append(can2d)
        ican = 0
        ih1 = -1
        for hname1 in hnames:
            ih1 = ih1 + 1
            ih2 = -1
            for hname2 in hnames:
                ih2 = ih2 + 1
                if ih2 >= ih1:
                    continue
                ican = ican + 1
                can2d.cd(ican)
                name = 'h2_{}_vs_{}{}'.format(hname2, hname1, chtag)
                for ich in range(0, nCh):
                    histos2d[name].Draw('colz')
                    DrawRho(histos2d[name])
        can2d.Update()


    # 2D for each variable between channels:
    cansVars2d = {}
    for hname in hnames:
        canname = 'canVar2d_{}'.format(hname)
        cw = 1200
        ch = 1200
        print('Creating canvas {}'.format(canname))
        cansVars2d[hname] = ROOT.TCanvas(canname, canname, 0, 0, cw, ch)
        nn = 4
        nx, ny = nn-1, nn-1 # HARDCODED!
        cansVars2d[hname].Divide(nx, ny)
        for ich1 in range(0, nCh):
            chtag1,chtit1 = GetChannelTagAndTitle(ich1)
            for ich2 in range(0, nCh):
                if ich2 >= ich1:
                    continue
                chtag2,chtit2 = GetChannelTagAndTitle(ich2)
                name = 'h2_{}{}_vs_{}{}'.format(hname, chtag1, hname, chtag2)
                #cansVars2d[hname].cd( nx*ich2 + (ich1 % ny) + 1) # worked with nn = 5
                #cansVars2d[hname].cd( nx*ich2 + ich1 +  1) # worked with nn = 5
                ind =  nx*ich2 + ich1
                cansVars2d[hname].cd(ind)
                histosVar2d[name].Draw('colz')
                DrawRho(histosVar2d[name])
        gcans.append(cansVars2d[hname])
        cansVars2d[hname].Update()

    # print png/pnd
    print(gcans)
    for can in gcans:
        #print(can)
        can.Print(can.GetName() + '.png')
        can.Print(can.GetName() + '.pdf')
    
    ROOT.gApplication.Run()
    return

###################################
###################################
###################################

if __name__ == "__main__":
    # execute only if run as a script"
    main(sys.argv)
    
###################################
###################################
###################################

