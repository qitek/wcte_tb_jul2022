#!/bin/bash

echo "Plotting..."
./scripts/plotAll.sh
echo "Making html pages..."
./scripts/makeHtml.sh
echo "Tarring..."
./scripts/tarPlotsHtml.sh

echo "Now you can see the html pages!"
echo "firefox index.html"

