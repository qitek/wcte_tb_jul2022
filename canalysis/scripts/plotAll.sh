#!/bin/bash

for i in `ls histos*.root` ; do
    echo $i
    python python/plotHistos.py  $i -b
done

echo "Now you can see the html pages!"
echo "firefox index.html"

