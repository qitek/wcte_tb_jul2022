#!/usr/bin/python

import os, sys

ddir='data/'

tag = ''
grepcmd = ''
if len(sys.argv) > 1:
    tag = sys.argv[1]
    grepcmd = ' | grep {}'.format(tag)

for xrfile in os.popen('cd {}  ; ls root_*.root {}'.format(ddir, grepcmd)):
    rfile = xrfile[:-1]
    #print(rfile)
    tokens = rfile.replace('.root','').split('_')
    run = tokens[2]
    clean = ''
    if len(tokens) > 4:
        clean = tokens[4]
    if len(clean) > 0:
        clean = '_' + clean 
    #print("Running over {} ...".format(run))
    cmd = "./bin/AnalyzeTB_x {}/root_run_{}_0000{}.root histos_run_{}_0000{}.root".format(ddir, run, clean, run, clean)
    print(cmd)

