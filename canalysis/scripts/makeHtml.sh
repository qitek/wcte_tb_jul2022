#!/bin/bash

for i in `ls | grep plots_` ; do

    if [ -d $i ] ; then
	run=`echo $i | sed "s/plots_//g"`
	cd $i
          ../scripts/CreateHTMLpage_noConv.sh $run
	cd -
    fi
done

file=index.html
title="WCTE TB July 2022"

echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\""  > ${file}
echo "   \"http://www.w3.org/TR/html4/loose.dtd\">"                      >> ${file}

echo "<html>" >> ${file}

echo "<head>"                      >> ${file}
echo "  <title> ${title} </title>" >> ${file}
echo "</head>"                     >> ${file}



echo " <body>" >> ${file}
echo "  <center><h1><span style=\"color: ${titlecolor};\"> ${title} </span></h1></center>" >> ${file}
echo "<p>" >> ${file}
echo ${text} >> ${file}

echo "</p>" >> ${file}


echo "  <table>" >> ${file}

for i in `ls plots_*/index.html` ; do
    echo "   <tr>" >> ${file}
    echo "   <td>" >> ${file}
    echo "   <a href="$i"> $i </a>" >> ${file}
    echo "   </td>" >> ${file}
    echo "    </tr>" >> ${file}
done
    
echo "  </table>" >> ${file}
echo "</center>" >> ${file}

DATE=`date`
echo "  <br /><br />Created ${DATE} by (c) ${USER} using <a href=\"$0\">$0</a> :)"  >> ${file}
echo " </body>" >> ${file}
echo "</html>" >> ${file}

echo "Now you can see the html pages!"
echo "firefox index.html"

