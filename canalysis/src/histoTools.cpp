#include "TH2.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TFile.h"
#include "TCanvas.h"

#include "midas.h"

void midas::PrintMap2() {
  cout << "... map2 ..." << endl;
  for (auto item : m_map_h2) {
     auto key = item.first;
     auto val = m_map_h2[key];
     cout << "key: \"" << key << "\" val: " << val << " name: " << val -> GetName() << endl;
   }
}

void midas::PrintMap1() {
  cout << "... map1 ..." << endl;
  for (auto item : m_map_h1) {
     auto key = item.first;
     auto val = m_map_h1[key];
     cout << "key: \"" << key << "\" val: " << val << " name: " << val -> GetName() << endl;
   }
}

void midas::PrintMapGr() {
  cout << "... map_gr ..." << endl;
  for (auto item : m_map_gr) {
     auto key = item.first;
     auto val = m_map_gr[key];
     cout << "key: \"" << key << "\" val: " << val  << " name: " << val -> GetName() << endl;
   }
}




// ________________________________________________________________________

TString midas::getStr(int i, int j) {
  return TString(Form("%i_%i", i, j));
}

// ________________________________________________________________________

void midas::MakeChannelHistos() {

 for (int ich = 0; ich < nChannels; ++ich) {
     TString name = Form("amplitude_%i", ich);
     TString title = name + Form(";Amplitude Ch%i", ich);
     m_h_ampl[ich] = new TH1D(name, title, nbins/2, minA, maxA*amplification(ich));
     
     name = Form("charge_%i", ich);
     title = name + Form(";Charge Ch%i", ich);
     m_h_charge[ich] = new TH1D(name, title, 1000, minch*amplification(ich), maxch*amplification(ich));

     name = Form("CFD_time_%i", ich);
     title = name + Form(";t_{CFD} Ch%i", ich);
     m_h_CFD_time[ich] = new TH1D(name, title, nbins, chtmin, chtmax);

     name = Form("global_time_%i", ich);
     title = name + Form(";t_{global} Ch%i", ich);
     m_h_global_time[ich] = new TH1D(name, title, nbins, chtmin, chtmax);

   }

}

// ________________________________________________________________________

void  midas::makeCorrHistos(int i1, int i2) {
  
  TString name = Form("h2_corr_amplitude_%i_%i", i1, i2);
  TString title = name + Form(";A_{%i};A_{%i}", i1, i2);
  TH2D *corrH_ampl = new TH2D(name, title, 100, 0, 250*amplification(i1), 100, 0, 250*amplification(i2));
  
  name = TString(Form("h2_corr_CFD_time_%i_%i", i1, i2));
  title = name + Form(";t^{CFD}_{%i};t^{CFD}_{%i}", i1, i2);
  TH2D *corrH_time = new TH2D(name, title, 100, tmin, tmax, 100, tmin, tmax);

  name = TString(Form("h2_corr_charge_%i_%i", i1, i2));
  title = name + Form(";charge_{%i};charge_{%i}", i1, i2);
  TH2D *corrH_charge = new TH2D(name, title,
				100, minch*amplification(i1), maxch*amplification(i1),
				100, minch*amplification(i2), maxch*amplification(i2));

  TString mapstr = "h2_" + getStr(i1, i2) + "_corr";
  m_map_h2[mapstr + "_ampl"] = corrH_ampl;
  m_map_h2[mapstr + "_time"] = corrH_time;
  m_map_h2[mapstr + "_charge"] = corrH_charge;
  //m_map_h2.insert(std::pair<const char*,TH2D*>((mapstr + "_charge", corrH_charge));
  //  cout << "\"" << mapstr + "_charge" << "\"" << endl;

  //this -> PrintMap2();
  
  return;
  
}
// ________________________________________________________________________
void midas::FillCorrHistos(int i1, int i2, double *amplitude, double *global_time, double *CFD_time, double *charge) {

  //  this -> PrintMap2();
  TString mapstr = "h2_" + getStr(i1, i2) + "_corr";  
  m_map_h2[mapstr + "_ampl"] -> Fill(amplitude[i1], amplitude[i2]);
  m_map_h2[mapstr + "_time"] -> Fill(CFD_time[i1], CFD_time[i2]);
  //  cout << "\"" << mapstr + "_charge" << "\"" << endl;
  m_map_h2[mapstr + "_charge"] -> Fill(charge[i1], charge[i2]);
}


// ________________________________________________________________________
void  midas::makeDiffHistos(int i1, int i2) {  
  
  TString name = Form("h1_diff_amplitude_%i_%i", i1, i2);
  TString title = name + Form(";A_{%i}-A_{%i}", i1, i2);
  TH1D *diffH_ampl = new TH1D(name, title, 100, -125.*amplification(i1), 125.*amplification(i1));

 
  name = Form("gr_diff_amplitude_%i_%i", i1, i2);
  title = name + Form(";Event number;A_{%i}-A_{%i}", i1, i2);
  TGraph *diffG_ampl = new TGraph();
  diffG_ampl -> SetName(name);
  diffG_ampl -> SetTitle(title);
  
  name = Form("h1_diff_CFD_time_%i_%i", i1, i2);
  title = name + Form(";t^{CFD}_{%i}-t^{CFD}_{%i}", i1, i2);
  TH1D *diffH_time = new TH1D(name, title, 100, dtmin, dtmax);

  name = Form("h1_diff_charge_%i_%i", i1, i2);
  title = name + Form(";charge_{%i}-charge_{%i}", i1, i2);
  TH1D *diffH_charge = new TH1D(name, title, 200, dchmin*amplification(i1), dchmax*amplification(i1));
  
  TString mapstr = "h1_" + getStr(i1, i2) + "_diff";
  m_map_h1[mapstr + "_ampl"] = diffH_ampl;
  m_map_gr[mapstr + "_ampl"] = diffG_ampl;
  m_map_h1[mapstr + "_time"] = diffH_time;
  m_map_h1[mapstr + "_charge"] = diffH_charge;
  
  return;
  
}
// ________________________________________________________________________
void midas::FillDiffHistos(int i1, int i2, Long64_t jentry, double *amplitude, double *global_time, double *CFD_time, double *charge) {
  TString mapstr = "h1_" + getStr(i1, i2) + "_diff";
  m_map_h1[mapstr + "_ampl"] -> Fill(amplitude[i1] - amplitude[i2]);
  m_map_h1[mapstr + "_time"] -> Fill(CFD_time[i1] - CFD_time[i2]);
  m_map_h1[mapstr + "_charge"] -> Fill(charge[i1] - charge[i2]);
  int ip = m_map_gr[mapstr + "_ampl"] -> GetN();
  m_map_gr[mapstr + "_ampl"] -> SetPoint(ip, jentry, amplitude[i1] - amplitude[i2]);
}


// ________________________________________________________________________

void  midas::makeSumHistos(int i1, int i2) {

  double t0 = 256./2.;
  double tw = 64./2.;
  double tmax = t0 + tw;
  //  double tmin = t0 - tw;

  int SF = fabs(1.*(i2 - i1)) + 1;
  
  TString name = Form("h1_sum_amplitude_%i_%i", i1, i2);
  TString title = name + Form(";A_{%i}+A_{%i}", i1, i2);
  TH1D *sumH_ampl = new TH1D(name, title, 100, 0, 250 * amplification(i1) * SF);
  
  // name = Form("h1_sum_CFD_time_%i_%i", i1, i2);
  // title = name + Form(";t^{CFD}_{%i}+t^{CFD}_{%i}", i1, i2);
  // TH1D *sumH_time = new TH1D(name, title, 100, tmin, tmax);

  double chmin = -5000.*amplification(i1) * SF;
  double chmax =  5000.*amplification(i1) * SF;
  name = Form("h1_sum_charge_%i_%i", i1, i2);
  title = name + Form(";charge_{%i}+charge_{%i}", i1, i2);
  TH1D *sumH_charge = new TH1D(name, title, 1000, chmin, chmax);
  
  TString mapstr = "h1_" + getStr(i1, i2) + "_sum";
  m_map_h1[mapstr + "_ampl"] = sumH_ampl;
  //m_map_h1[mapstr + "_time"] = sumH_time;
  m_map_h1[mapstr + "_charge"] = sumH_charge;
  
  return;
  
}
// ________________________________________________________________________
void midas::FillSumHistos(int i1, int i2, double *amplitude, double *global_time, double *CFD_time, double *charge) {
  TString mapstr = "h1_" + getStr(i1, i2) + "_sum";
  m_map_h1[mapstr + "_ampl"] -> Fill(amplitude[i1] + amplitude[i2]);
  //  m_map_h1[mapstr + "_time"] -> Fill(CFD_time[i1] + CFD_time[i2]);
  m_map_h1[mapstr + "_charge"] -> Fill(charge[i1] + charge[i2]);
}


// ________________________________________________________________________
double midas::computeSum(double *darray, int i1, int i2) {
  double sum = 0.;
  //cout << "computeSum i1=" << i1 << " i2=" << i2 << endl;
  for (int i = i1; i <= i2; ++i) {
    //cout << "adding " << darray[i] << endl;
    sum += darray[i];
  }
  //cout << "...sum: " << sum << endl;
  return sum;
}

// ________________________________________________________________________
double midas::computeAverage(double *darray, int i1, int i2) {
  double aver = 0.;
  int n = 0;
  for (int i = i1; i <= i2; ++i) {
    aver += darray[i];
    n++;
  }
  return aver / n;
}

// ________________________________________________________________________

double midas::computeTOF(double *CFD_time) {
  /*
  double tof0 = computeAverage(CFD_time, 8, 11);
  double tof1 = computeAverage(CFD_time, 12, 15);
  return 1./1.60*(tof1 - tof0); // ns
  */
  double tof0 = computeSum(CFD_time, 8, 11);
  double tof1 = computeSum(CFD_time, 12, 15);
  //return 1./1.60 * (tof1 - tof0) / 4.; // ns
  return (tof1 - tof0) / 4.; // ns
  
}

// ________________________________________________________________________
double midas::computeACTaverageCharge(double *charges, int i1, int i2) {
  return computeAverage(charges, i1, i2);
}

// ________________________________________________________________________
double midas::computeACTSumAmpl(double *ampl, int i1, int i2) {
  return computeSum(ampl, i1, i2);
}


// ________________________________________________________________________
void midas::MakeACTHistos() {
  
  for (int icut = 0; icut < m_nCuts; ++icut) {
    
    TString cutname = m_cutnames[icut];
    //this -> PrintMap1();
    m_map_h1["h1_t_TOF" + cutname] = new TH1D("t_TOF" + cutname, "t_TOF" + cutname + ";t_{TOF} [ns];", ntofbins, tofmin, tofmax);
    //this -> PrintMap1();
    TString varnames[2] = {"SumAmpl", "AverCharge"};
    for (auto varname : varnames ) {

      double xmin = minch;
      double xmax = maxch;
      if (varname.Contains("Ampl")) {
	xmin = minA;
	xmax = maxA;
      }
      //if (varname.Contains("Sum")) {
      //xmin *= nActs;
      //xmax *= nActs;
      //}

      for (int i = 0; i < nActs; ++i) {

	TString name = m_actStr[i] + varname + cutname;
	TString title = m_actTitle[i] + ";" + varname + " " + m_actStr[i];
	double amplif1 = 10.;
	//if (varname.Contains("Charge")) {
	if (i > 0)
	  amplif1 = 1.;
	//}
	//cout << "name: " << name.Data() << endl;
	m_map_h1["h1_" + name] = new TH1D(name, title, 200, xmin*amplif1, xmax*amplif1);
       	//this -> PrintMap1();
	name = m_actStr[i] + varname + "_vs_t_TOF" + cutname;
	title = varname + " " + m_actTitle[i] + " vs t_{TOF};t_{TOF} [ns];" + varname + " " + m_actStr[i];
	//cout << "tofmin=" << tofmin << " tofmax=" << tofmax << endl;
	//cout << "amplitude ranges for h2 vs tof: " << xmin*amplif1 << " " <<  xmax*amplif1 << endl;
	m_map_h2["h2_" + name] = new TH2D(name, title, ntofbins, tofmin, tofmax, 100, xmin*amplif1, xmax*amplif1);
	//this -> PrintMap2();
	//	cout << "booked " << m_map_h2["h2_"+name]->GetName() << " with x-axis: "<< m_map_h2["h2_"+name] -> GetNbinsX() << " "
	//    << m_map_h2["h2_"+name] ->GetXaxis()->GetXmin() << " " << m_map_h2["h2_"+name] ->GetXaxis()->GetXmax() << endl;
	for (int j = 0; j < nActs; ++j) {
	  if (j >= i)
	    continue;
	  double amplif2 = 10.;
	  //if (varname.Contains("Charge")) {
	  if (j > 0)
	    amplif2 = 1.;
	  //}
	  name = m_actStr[j] + varname + "_vs_" + m_actStr[i] + varname + cutname;
	  title = varname + " " + m_actTitle[j] + " vs " + varname + " " + m_actTitle[i] + ";" + varname + " " + m_actStr[i] + "; " + varname + " " + m_actStr[j];
	  m_map_h2["h2_" + name] = new TH2D(name, title, 100, xmin*amplif1, xmax*amplif1,  100, xmin*amplif2, xmax*amplif2);
	  //this -> PrintMap2();
	} // act j
	//this -> PrintMap2();
      } // act i
    } // varnames
  } // cuts
}

// ________________________________________________________________________

void midas::FillACTTOFHistos(TString cutname, double t_TOF, double *ACTAverCharge, double *ACTSumAmpl) {

  m_map_h1["h1_t_TOF" + cutname] -> Fill(t_TOF);
  
  for (int i = 0; i < nActs; ++i) {
    m_map_h1["h1_" + m_actStr[i] + "AverCharge" + cutname] -> Fill(ACTAverCharge[i]);
    m_map_h2["h2_" + m_actStr[i] + "AverCharge_vs_t_TOF" + cutname] -> Fill(t_TOF, ACTAverCharge[i]);
    for (int j = 0; j < nActs; ++j) {
      if (j >= i)
	continue;
      m_map_h2["h2_" + m_actStr[j] + "AverCharge_vs_" + m_actStr[i] + "AverCharge" + cutname] -> Fill(ACTAverCharge[i], ACTAverCharge[j]);
    }
  }

  //cout << "--------------------------------------------------------------------------------------------------" << endl;
  for (int i = 0; i < nActs; ++i) {

    m_map_h1["h1_" + m_actStr[i] + "SumAmpl" + cutname] -> Fill(ACTSumAmpl[i]);


    TString name = "h2_" + m_actStr[i] + "SumAmpl_vs_t_TOF"+ cutname;
    //if (i == 0 && cutname == "") {
    /*
    cout << "i: " << i << " cut: \"" << cutname.Data() << "\", filling "
	 << m_map_h2[name.Data()] << " "
	 << m_map_h2[name.Data()]->GetName()
      	 << " meanx: " << m_map_h2[name.Data()] ->GetMean(1)
	 << " meany: " << m_map_h2[name.Data()] ->GetMean(2)
	 << " Entrs: " << m_map_h2[name.Data()] ->GetEntries()
	 << " Axes: "
	 << m_map_h2[name.Data()] -> GetNbinsX() << " "
	 << m_map_h2[name.Data()] ->GetXaxis()->GetXmin() << " "
	 << m_map_h2[name.Data()] ->GetXaxis()->GetXmax()
	 << endl;
      //}
      */
    m_map_h2["h2_" + m_actStr[i] + "SumAmpl_vs_t_TOF" + cutname] -> Fill(t_TOF, ACTSumAmpl[i]);
    /*
    cout << "i: " << i << " cut: \"" << cutname.Data() << "\", filled  "
	 << m_map_h2[name.Data()] << " "
	 << m_map_h2[name.Data()]->GetName()
      	 << " meanx: " << m_map_h2[name.Data()] ->GetMean(1)
	 << " meany: " << m_map_h2[name.Data()] ->GetMean(2)
	 << " Entrs: " << m_map_h2[name.Data()] ->GetEntries()
	 << " Axes: "
	 << m_map_h2[name.Data()] -> GetNbinsX() << " "
	 << m_map_h2[name.Data()] ->GetXaxis()->GetXmin() << " "
	 << m_map_h2[name.Data()] ->GetXaxis()->GetXmax()
	 << endl;
    */
    
    for (int j = 0; j < nActs; ++j) {
      if (j >= i)
	continue;
      name = "h2_" + m_actStr[j] + "SumAmpl_vs_" + m_actStr[i] + "SumAmpl" + cutname;
      m_map_h2[name.Data()] -> Fill(ACTSumAmpl[i], ACTSumAmpl[j]);
    }
  }

  
}

// ________________________________________________________________________
// ________________________________________________________________________
// ________________________________________________________________________
