#define midasClass_cxx
#include <iostream>
#include "midasClass.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>


midasClass::midasClass(TFile *infile, TString treename) : fChain(0) 
{
  TTree *tree = (TTree*) infile -> Get(treename);
//   std::cout << tree << std::endl;
  Init(tree);
}

midasClass::~midasClass()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t midasClass::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}

Long64_t midasClass::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void midasClass::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   Channel0 = 0;
   Channel1 = 0;
   Channel2 = 0;
   Channel3 = 0;
   Channel4 = 0;
   Channel5 = 0;
   Channel6 = 0;
   Channel7 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("midasEvent", &midasEvent, &b_midasEvent);
   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("triggerTime", &triggertime, &b_triggerTime);
   //fChain->SetBranchAddress("serialnumber", &serialnumber, &b_serialnumber);
   fChain->SetBranchAddress("freqsetting", &freqsetting, &b_freqsetting);
   fChain->SetBranchAddress("Channel0", &Channel0, &b_Channel0);
   fChain->SetBranchAddress("Channel1", &Channel1, &b_Channel1);
   fChain->SetBranchAddress("Channel2", &Channel2, &b_Channel2);
   fChain->SetBranchAddress("Channel3", &Channel3, &b_Channel3);
   fChain->SetBranchAddress("Channel4", &Channel4, &b_Channel4);
   fChain->SetBranchAddress("Channel5", &Channel5, &b_Channel5);
   fChain->SetBranchAddress("Channel6", &Channel6, &b_Channel6);
   fChain->SetBranchAddress("Channel7", &Channel7, &b_Channel7);
   Notify();
}

Bool_t midasClass::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void midasClass::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t midasClass::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

void midasClass::Loop()
{
}
