#define midas_cxx
#include "midas.h"
#include "TH2.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TFile.h"
#include "TCanvas.h"

#include "TROOT.h"
#include "TApplication.h"


// Luan
// conversion to mV
#define mppc_DIGITIZER_FULL_SCALE_RANGE 2.5 // Vpp
#define mppc_DIGITIZER_RESOLUTION       12 // bits

// converting from sample number to ns I use the following scale factor:
#define mppc_DIGITIZER_SAMPLE_RATE 800000000 // Sa/S


// ________________________________________________________________________
midas::midas(TString infilename, TString outfilename) {

  m_outfilename = outfilename;
  m_infilename = infilename;

  m_infile = new TFile(m_infilename, "read");
  
  m_midasClass1 = new midasClass(m_infile, "midas_data1");
  m_midasClass2 = new midasClass(m_infile, "midas_data2");

  // binning

  nbins = 250;
  chtmax = 512;
  chtmin = 0;
  
  tw = 32.;
  dtmax = +tw;
  dtmin = -tw;

  dchmin = -1000;
  dchmax = 1000;

  // for diffs
  t0 = 256.;
  tmax = t0;
  tmin = 0.;

  //  ntofbins = 42;
  // default:
  //tofmin = 38.;//61 / 1.6;
  //tofmax = 42.;//67 / 1.6;

  ntofbins = 30;
  tofmin = 62.;
  tofmax = 68.;
  
  minch = -5000;
  maxch = 5000;

  minA = 0.;
  maxA = 250.;

  m_nCuts = 4;
  m_cutnames[0] = "_noCuts";
  //m_cutnames[1] = "_ACT0AverChargeCut";
  m_cutnames[1] = "_ACT0SumAmplCut";
  m_cutnames[2] = m_cutnames[1] + "_ACT1SumAmplCut";
  m_cutnames[3] = m_cutnames[2] + "_ACT2SumAmplCut";

  m_actStr[0] = "ACTe";
  m_actStr[1] = "ACTmu";
  m_actStr[2] = "ACTtau";

  m_actTitle[0] = "ACT n=1.06";
  m_actTitle[1] = "ACT n=1.10";
  m_actTitle[2] = "ACT n=1.15";


  
}


 // ________________________________________________________________________
 // ________________________________________________________________________
 // ________________________________________________________________________

void midas::Loop(int argc, char *argv[], int debug)
{

  // To convert vertical scale from ADC units to mV 
  double digiCounts = pow(2.0, mppc_DIGITIZER_RESOLUTION); // counts
  double verticalScaleFactor = 1.e3*mppc_DIGITIZER_FULL_SCALE_RANGE/digiCounts; // V / counts

  // converting from sample number to ns I use the following scale factor:
  double horizontalScaleFactor = 1.e9/mppc_DIGITIZER_SAMPLE_RATE; // ns/sample;

   // output file, histograms, TTree for later playing with
   m_outfile = new TFile(this->m_outfilename, "recreate");
   TTree* tree = new TTree("events", "events");

   int nCh;
   double CFD_time[nChannels];
   double global_time[nChannels];
   ULong_t trigger_time1;
   ULong_t trigger_time2;
   double amplitude[nChannels];
   double charge[nChannels];
   double t_TOF;
   double ACTAverCharge[nActs];
   double ACTSumAmpl[nActs];

   double CFD_amplitude; // to store in TTRee?
   
   tree -> Branch("nCh", &nCh, "nCh/I" );    
   tree -> Branch("CFD_time", &CFD_time,  "CFD_time[nCh]/D");
   tree -> Branch("global_time", &global_time, "global_time[nCh]/D");
   tree -> Branch("triggerTime1", &trigger_time1, "triggerTime1/l");
   tree -> Branch("triggerTime2", &trigger_time2, "triggerTime2/l"); 
   tree -> Branch("amplitude", &amplitude,  "amplitude[nCh]/D");
   tree -> Branch("charge", &charge, "charge[nCh]/D");
   tree -> Branch("t_TOF", &t_TOF, "t_TOF/D");
   tree -> Branch("ACTAverCharge", &ACTAverCharge, "ACTAverCharge[3]/D");
   tree -> Branch("ACTSumAmpl", &ACTSumAmpl, "ACTSumAmpl[3]/D");

   
   //const int number_samples = 256; //512; // taken later from the number of bins of waveforms

   this -> MakeACTHistos();
   
   this -> MakeChannelHistos();

   // selected correlation histos between selected channels
   makeCorrHistos(7, 11);
   makeCorrHistos(8, 9);
   makeCorrHistos(0, 1);

   // selected diff histos between selected channels
   makeDiffHistos(7, 11);
   makeDiffHistos(8, 9);
   makeDiffHistos(0, 1);

   // selected sum histos between selected channels
   makeSumHistos(0, 1);
   makeSumHistos(2, 3);
   makeSumHistos(4, 5);

   TApplication *app = 0;
   //TApplication *app = new TApplication(TString("dumm").Data(), &argc, argv);
   
  ////////////////
  //    LOOOP   //
  ////////////////
  
   Long64_t nentries1 = m_midasClass1 -> fChain->GetEntries();
   Long64_t nentries2 = m_midasClass2 -> fChain->GetEntries();
   Long64_t nentries = nentries1;
   if (nentries2 != nentries1) {
     cerr << "ERROR! Different number of events in the two midas trees! " << nentries1 << " vs " << nentries2  << endl;
     cout << "Will run over min(entr1,entr2)" << endl;
     nentries = nentries1 < nentries2 ? nentries1 : nentries2;
   }

   int verbose = 1000;
   Long64_t nbytes1 = 0, nb1 = 0;
   Long64_t nbytes2 = 0, nb2 = 0;

   // HACK!
   //   nentries = 1100;
   for (Long64_t jentry=0; jentry < nentries; jentry++) {

     Long64_t ientry1 = m_midasClass1 ->LoadTree(jentry);
     if (ientry1 < 0) break;
     nb1 = m_midasClass1 -> fChain->GetEntry(jentry);
     nbytes1 += nb1;

     Long64_t ientry = m_midasClass2 ->LoadTree(jentry);
     if (ientry < 0) break;
     nb2 = m_midasClass2 -> fChain -> GetEntry(jentry);
     nbytes2 += nb2;

     // if (Cut(ientry) < 0) continue;
     
     if (jentry % verbose == 0) {
       if (!debug)
	 cout << "\r* " << jentry << "/" << nentries << std::flush; // << endl;
       else
	 cout << "* " << jentry << "/" << nentries << endl;
     }
      TH1D *Waveforms[] = {m_midasClass1->Channel0,   m_midasClass1->Channel1,  m_midasClass1->Channel2,  m_midasClass1->Channel3,
			   m_midasClass1->Channel4,   m_midasClass1->Channel5,  m_midasClass1->Channel6,  m_midasClass1->Channel7,
			   m_midasClass2->Channel0,   m_midasClass2->Channel1,  m_midasClass2->Channel2,  m_midasClass2->Channel3,
			   m_midasClass2->Channel4,   m_midasClass2->Channel5,  m_midasClass2->Channel6,  m_midasClass2->Channel7};

      trigger_time1 = m_midasClass1->triggertime;
      trigger_time2 = m_midasClass2->triggertime;
      if (debug) std::cout << "Trigger 1: " << m_midasClass1->triggertime<<std::endl;
      if (debug) std::cout << "Trigger 2: " << m_midasClass2->triggertime<<std::endl;

      for(int ich = 0; ich < nChannels; ++ich) {
	
	TH1D *waveform = Waveforms[ich];
	if (app) {
	  TString canname = "testcan";
	  TCanvas *can = new TCanvas(canname,canname);
	  can -> Draw();
	  waveform -> Draw("hist");
	  can -> Print(TString(can->GetName()) + ".png");
	  app -> Run();
	}
	
	
	int number_samples = waveform -> GetNbinsX();
	
	// conversion to mV!
	waveform -> Scale(verticalScaleFactor);
	double baseline = getBaseline(waveform);

	// Luan:
	const float start_rise_percentage = 0.1;
	const float end_rise_percentage = 0.9;
	const float percentage = 0.2;

	int iglobal = global_timing(waveform, number_samples);
	global_time[ich] = 1.*iglobal;
	m_h_global_time[ich] -> Fill(global_time[ich]);
	if (debug) cout << "=== iglobal: " << iglobal << endl;
	CFD_time[ich] = CFD_timing(waveform, baseline, iglobal, start_rise_percentage, end_rise_percentage, percentage, CFD_amplitude);
	m_h_CFD_time[ich] -> Fill(CFD_time[ich]);
	
	amplitude[ich] = getAmplitude(waveform, baseline, iglobal);
	m_h_ampl[ich] -> Fill(amplitude[ich]);

	charge[ich] = horizontalScaleFactor*getCharge(waveform, baseline, initialBinForCharge, number_samples);
	m_h_charge[ich] -> Fill(charge[ich]);

	if (debug) cout << "    ich: " << ich <<  " baseline: " << baseline << " charge: " << charge[ich] << endl;
	nCh = nChannels;

	if (debug) cout << "    amplitude: " << amplitude[ich] << " iglobal: " << iglobal << " tglob: " << global_time[ich] << " tCFD: " << CFD_time[ich] << endl;
	
      } // channels

      // for the channels map see
      // https://docs.google.com/spreadsheets/d/1scoCYNb7RodWaWawr3xai6wEr4tchfL2M68FREvPNk4/edit?usp=sharing

      // averaged ACT charges
      ACTAverCharge[0]   = this -> computeACTaverageCharge(charge, 0, 1);
      ACTAverCharge[1]  = this -> computeACTaverageCharge(charge, 2, 3);
      ACTAverCharge[2] = this -> computeACTaverageCharge(charge, 4, 5);

      ACTSumAmpl[0]   = this -> computeACTSumAmpl(amplitude, 0, 1);
      ACTSumAmpl[1]  = this -> computeACTSumAmpl(amplitude, 2, 3);
      ACTSumAmpl[2] = this -> computeACTSumAmpl(amplitude, 4, 5);
      
      // compute the TOF
      t_TOF = this -> computeTOF(CFD_time);

      // corr histos
      this -> FillCorrHistos(7, 11, amplitude, global_time, CFD_time, charge);
      this -> FillCorrHistos(8, 9, amplitude, global_time, CFD_time, charge);
      this -> FillCorrHistos(0, 1, amplitude, global_time, CFD_time, charge);

      // diff histos
      this -> FillDiffHistos(7, 11, jentry, amplitude, global_time, CFD_time, charge);
      this -> FillDiffHistos(8, 9, jentry, amplitude, global_time, CFD_time, charge);
      this -> FillDiffHistos(0, 1, jentry, amplitude, global_time, CFD_time, charge);
      
      // sum
      this -> FillSumHistos(0, 1, amplitude, global_time, CFD_time, charge);
      this -> FillSumHistos(2, 3, amplitude, global_time, CFD_time, charge);
      this -> FillSumHistos(4, 5, amplitude, global_time, CFD_time, charge);

      // ACT 2D and vs. t_TOF
      this -> FillACTTOFHistos(m_cutnames[0], t_TOF, ACTAverCharge, ACTSumAmpl);
      
      // cut on the ACTs, to remove electrons
      //      if (ACTAverCharge[0] > -15000.) {
      if (ACTSumAmpl[0] < 250.) {
	this -> FillACTTOFHistos(m_cutnames[1], t_TOF, ACTAverCharge, ACTSumAmpl);
	
	if (ACTSumAmpl[1] < 35.) { // 20; 50
	  this -> FillACTTOFHistos(m_cutnames[2], t_TOF, ACTAverCharge, ACTSumAmpl);

	  if (ACTSumAmpl[2] < 60.) { // 50; 70
	    this -> FillACTTOFHistos(m_cutnames[3], t_TOF, ACTAverCharge, ACTSumAmpl);
	  } // cut on ACT1.15
	} // cut on ACT1.10
      } // cut on ACT1.06
      

      tree -> Fill();
      
   } // loop
   cout << "\rDone the loop!" << endl;
   
   for (auto item : m_map_gr) {
     auto key = item.first;
     auto val = m_map_gr[key];
     val -> Write();
   }
   
   m_outfile->Write();
   m_outfile->Close();

   cout << "DONE!" << endl;
   cout << "Consider:" << endl;
   cout << "python ./python/plotHistos.py " << m_outfilename.Data() << endl;
}


 // ________________________________________________________________________
 // ________________________________________________________________________
 // ________________________________________________________________________
