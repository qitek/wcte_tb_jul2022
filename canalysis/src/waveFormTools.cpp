#include "TH2.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TFile.h"
#include "TCanvas.h"

#include "midas.h"

// ________________________________________________________________________

double midas::amplification(int ich) {
  if (ich >= 2 && ich <= 6)
    return 1.;
  return 10.;
}



// ________________________________________________________________________
// Jiri
double midas::getAmplitude(TH1D *h, double baseline, int imin)
{
  double ymin = h -> GetBinContent(imin);
  double A = baseline - ymin;
  //cout << "nbins: " << h->GetNbinsX() << " imin: " << imin << " ymin: " << ymin << " baseline: " << baseline << " A:" << A << endl;
  return A;
}

/*
// ________________________________________________________________________
// Jiri
int getGlobalTime(TH1D *h)
{
  double miny = 999e9;
  int imax = -1;
  double val = 2*miny;
  for (int k=1; k < h -> GetNbinsX()+1; k++) {
    val = h -> GetBinContent(k);
    if (val < miny) {
      miny = val;
      imax = k;
    }
  }
  return imax;
}
*/


// ________________________________________________________________________
// Luan
double midas::getBaseline(TH1D *waveform, int baseline_samples) {
  // Measure baseline for this channel
  double sample_sum = 0.;
  for (int k=1; k < baseline_samples+1; k++) {
    sample_sum += waveform -> GetBinContent(k);
  }
  return sample_sum / baseline_samples;
}

int midas::global_timing(TH1D* waveform, const int number_samples) {
    // Find global minimum
    double ymin = 9999.;
    int imin = 0;
    double val = 9999.;
    for (int k = 1; k < number_samples-1; k++){
      val = waveform->GetBinContent(k);
      if (val < ymin) {
        ymin = val;
        imin = k;
      }
    }
    return imin;
}


// Luan:

// Function to find CFD time of pulse.
double midas::CFD_timing(TH1D* waveform, double baseline, const int global_imin, const float startp, const float endp, const float percentage, double & CFD_amplitude) {
    double y_min = waveform->GetBinContent(global_imin)-baseline;
    double y_end = endp*y_min;
    double y_start = startp*y_min;
    double rise_amplitude = (endp-startp)*y_min;
    int j = global_imin;
    int j_end = 0;
    while (((waveform->GetBinContent(j)-baseline) < y_end) && (j > 1)) {
        j--;
        j_end = j-1;
    }
    j = global_imin;
    int j_start = 0;
    while (((waveform->GetBinContent(j)-baseline) < y_start) && (j > 1)) {
        j--;
        j_start = j+1;
    }
    double b = (y_end-y_start)/(double(j_end)-double(j_start));
    double a = y_start - j_start*b;
    double iCFD = (y_start+percentage*rise_amplitude-a)/b;
    CFD_amplitude = y_start+percentage*rise_amplitude;
    return iCFD;
}



/* old version:
float midas::CFD_timing(TH1D* waveform, double baseline, const int global_imin, const float startp, const float endp, const float percentage) {
    double y_min = waveform->GetBinContent(global_imin) - baseline;
    double y_end = endp*y_min;
    double y_start = startp*y_min;
    double rise_amplitude = (endp - startp)*y_min;
    // std::cout << y_min << "  " << y_end << "   " << y_start << std::endl;
    int j = global_imin;
    int j_end = 0;
    while (((waveform->GetBinContent(j)-baseline) < y_end) && (j > 1)) {
        j--;
        j_end = j;
        // std::cout << "while: " << global_imin << "   " << j << std::endl;
    }
    j = global_imin;
    int j_start = 0;
    while (((waveform->GetBinContent(j)-baseline) < y_start) && (j > 1)) {
        j--;
        j_start = j;
        // std::cout << "while: " << global_imin << "   " << j << std::endl;
    }
    double b = (y_end-y_start)/(double(j_end)-double(j_start));
    double a = y_start - j_start*b;
    double iCFD = (percentage*rise_amplitude-a)/b;
    return iCFD;
    // std::cout << global_imin << "  " << y_min << "  " << j_end << "  " << waveform->GetBinContent(j_end)-baseline << std::endl;
    // return 0.;
}
*/



// ________________________________________________________________________
// Luan
double midas::getCharge(TH1D* waveform, double baseline, const int A, const int B) {
    double charge = 0.;
    for (int j = A; j < B; j++) {
        charge += waveform->GetBinContent(j) - baseline;
    }
    return charge;
}
