#include "midas.h"

#include <iostream>
#include <fstream>
#include <list>
using std::cout;
using std::cerr;
using std::endl;

#include "TObjString.h"

// ________________________________________________________________________

int main(int argc, char *argv[])

{

    if (argc < 3) {
      cout << "Usage " << argv[0] << " data/inputfile.root histosfile.root" << endl;
      return -1;
    }

    TString infilename = TString(argv[1]);
    TString outfilename = TString(argv[2]);

    cout << "Configured as" << endl;
    cout << " input: " << infilename.Data() << endl;
    cout << " output: " << outfilename.Data() << endl;


    int debug = 0;
    midas* midasAnalysis = new midas(infilename, outfilename);
    midasAnalysis -> Loop(argc, argv, debug);
      
    delete midasAnalysis;
    return 0;
}

