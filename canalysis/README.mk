
## Running the analysis

cd /home/wcte/analysis/wcte_tb_jul2022/canalysis

List the link to the root files and search for latest runs there: ls -lhart data/

For the runno you want to check, run

python scripts/Run.py runno

You may get more commands to run, select what you want and run the desired one in terminal.

Code update and compile:
git pull && ./make.sh

Commands look like ./bin/AnalyzeTB_x data//root_run_000109_0000.root histos_run_000109_0000.root

The output is e.g. histos_run_000109_0000_clean.root

## Plotting
Run over your runno XYZ the script

python ./python/plotHistos.py histos_run_000XYZ_0000.root

Then create the html pages

./scripts/makeHtml.sh

Or, careful, you can plot all from histos*.root ./scripts/PlotAndHtmlAll.sh

Finally: firefox index.html

## Running over momenta

./python/GrandRunAllMomenta.py 0/1




