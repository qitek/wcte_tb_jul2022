#!/usr/bin/python3
# St 13. července 2022, 10:49:04 CEST
from collections import OrderedDict

import ROOT
from math import sqrt, pow, log, exp
import os, sys, getopt
import numpy as np
import csv
from array import array

cans = []
stuff = []

##########################################
# https://www.tutorialspoint.com/python/python_command_line_arguments.htm
def main(argv):
    #if len(sys.argv) > 1:
    #  foo = sys.argv[1]

    ### https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    ### https://pymotw.com/2/getopt/
    ### https://docs.python.org/3.1/library/getopt.html


    debug = 0
    gBatch = False
    gTag=''
    print(argv[1:])
    try:
        # options that require an argument should be followed by a colon (:).
        opts, args = getopt.getopt(argv[2:], 'hbt:', ['help','batch','tag='])

        print('Got options:')
        print(opts)
        print(args)
    except getopt.GetoptError:
        print('Parsing...')
        print ('Command line argument error!')
        print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]]'.format(argv[0]))
        sys.exit(2)
    for opt,arg in opts:
        print('Processing command line option {} {}'.format(opt,arg))
        if opt == '-h':
            print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]'.format(argv[0]))
            sys.exit()
        elif opt in ("-b", "--batch"):
            gBatch = True
        elif opt in ("-t", "--tag"):
            gTag = arg
            print('OK, using user-defined histograms tag for output pngs {:}'.format(gTag,) )

    if gBatch:
        ROOT.gROOT.SetBatch(1)

    print('*** Settings:')
    print('tag={:}, batch={:}'.format(gTag, gBatch))

    #canname = 'can'
    #can = ROOT.TCanvas(canname, canname)
    #cans.append(can)

    # later: read *{sometag}*.txt
    fnames = [ 'pulse_information-XYZ-0.1rise_time-dt5743_wave00.txt',
               'pulse_information-XYZ-0.1rise_time-dt5743_wave02.txt',
               'pulse_information-XYZ-0.1rise_time-dt5743_wave01.txt',
               'pulse_information-XYZ-0.1rise_time-dt5743_wave03.txt',
    ]


    asciifiles = []

    # read files
    data_arrays = OrderedDict()
    for fname in fnames:
        asciifiles.append(open(fname, 'r'))


        with open(fname,'r') as csvfile:
            data_iter = csv.reader(csvfile,
                                   delimiter = ',',
                                   quotechar = '"')
            data = [data for data in data_iter]
        data_array = np.asarray(data, dtype = 'float')
        data_arrays[fname] = data_array

    nlines = []
    for fname in data_arrays:
        nl = len(data_arrays[fname])
        print('Read {} of {} lines'.format(fname, nl))
        nlines.append(nl)
        # test:
        # print(data_arrays[fname][2])
    print('number of lines: {}'.format(nlines))
    
    # go through the data simultaneously and fill the TTree
    # to change to sometag.root
    rfilename = 'events_tree.root'
    rfile = ROOT.TFile(rfilename, 'recreate')
    # following https://wiki.physik.uzh.ch/cms/root:pyroot_ttree
    tree = ROOT.TTree('TOF', 'TOF')

    Nmax        = 16
    nCh         = array( 'i', [0] )
    CFD_time    = array('d', Nmax*[0])
    global_time = array('d', Nmax*[0])
    amplitude   = array('d', Nmax*[0])
    charge      = array('d', Nmax*[0])
    
    # create the branches and assign the fill-variables to them as doubles (D)
    tree.Branch('nCh', nCh, 'nCh/I' )    
    tree.Branch('CFD_time', CFD_time,  'CFD_time[nCh]/D')
    tree.Branch('global_time', global_time, 'global_time[nCh]/D')
    tree.Branch('amplitude', amplitude,  'amplitude[nCh]/D')
    tree.Branch('charge', charge, 'charge[nCh]/D')
    
    nls = min(nlines)

    print('Saving data to a TTree...')
    #print(nls, nCh)
    for iline in range(0,nls):
        ifile = -1
        if debug: print('Processing line {}'.format(iline))
        nCh[0] = 1*int(len(data_arrays))
        for fname in data_arrays:
            ifile = ifile + 1
            if debug: print('  processing file {}'.format(ifile))
            CFD_time[ifile]     = 1.*data_arrays[fname][iline][0]
            global_time[ifile]  = 1.*data_arrays[fname][iline][1]
            amplitude[ifile]    = 1.*data_arrays[fname][iline][2]
            charge[ifile]       = 1.*data_arrays[fname][iline][3]
            if debug: print('    ', nCh, CFD_time[ifile], global_time[ifile], amplitude[ifile], charge[ifile])
        if debug: print('  ...filling the TTree')
        tree.Fill()
        if debug: print('  ...done!')
        
    print('DONE!')

    rfile.Write()
    rfile.Close()    
    # ROOT.gApplication.Run()
    return

###################################
###################################
###################################

if __name__ == "__main__":
    # execute only if run as a script"
    main(sys.argv)
    
###################################
###################################
###################################

