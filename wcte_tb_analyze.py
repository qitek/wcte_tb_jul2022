#!/usr/bin/python3
# 12/07/2022 14:01:56 CEST


import ROOT
from math import sqrt, pow, log, exp
import os, sys, getopt

cans = []
stuff = []


def GetAmplitude(h):
    nb = h.GetXaxis().GetNbins()
    ymax = -1
    ymin = 999e9
    for ibin in range(1, nb+1):
        val = h.GetBinContent(ibin)
        if val > ymax:
            ymax = 1.*val
        if val < ymin:
            ymin = 1.*val
    return ymax - ymin



def SetOpt1d(h, col = ROOT.kBlack, fcol = ROOT.kCyan):
    h.SetLineColor(col)
    h.SetMarkerColor(col)
    h.SetLineStyle(1)
    h.SetLineWidth(1)
    if fcol > 0:
        h.SetFillColor(fcol)
        h.SetFillStyle(1111)
    h.SetMarkerStyle(20)
    h.SetMarkerSize(1)
    #h.SetMinimum(0.)
    #h.SetStats(0)


    
##########################################
# https://www.tutorialspoint.com/python/python_command_line_arguments.htm
def main(argv):
    #if len(sys.argv) > 1:
    #  foo = sys.argv[1]

    ### https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    ### https://pymotw.com/2/getopt/
    ### https://docs.python.org/3.1/library/getopt.html
    gBatch = False
    gTag=''
    print(argv[1:])
    try:
        # options that require an argument should be followed by a colon (:).
        opts, args = getopt.getopt(argv[2:], 'hbt:', ['help','batch','tag='])

        print('Got options:')
        print(opts)
        print(args)
    except getopt.GetoptError:
        print('Parsing...')
        print ('Command line argument error!')
        print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]]'.format(argv[0]))
        sys.exit(2)
    for opt,arg in opts:
        print('Processing command line option {} {}'.format(opt,arg))
        if opt == '-h':
            print('{:} [ -h -b --batch -tTag --tag="MyCoolTag"]'.format(argv[0]))
            sys.exit()
        elif opt in ("-b", "--batch"):
            gBatch = True
        elif opt in ("-t", "--tag"):
            gTag = arg
            print('OK, using user-defined histograms tag for output pngs {:}'.format(gTag,) )

    if gBatch:
        ROOT.gROOT.SetBatch(1)

    print('*** Settings:')
    print('tag={:}, batch={:}'.format(gTag, gBatch))

    

    ############
    #   Read   #
    ############
    
    if len(argv) < 2:
        print('Usage: {} rootfile.root'.format(argv[0]))
        return
    filename = argv[1]
    rfile = ROOT.TFile(filename, 'read')
    treename = 'midas_data'

    runtag = filename.split('_')[2]
    
    fchain = ROOT.TChain(treename)
    fchain.Add(filename)
    
    nEvts = fchain.GetEntries() 
    print('nEvts: {}'.format(nEvts))
    print(fchain)


    ############
    #   histogramming   #
    ############
    

    varnames = {#'CFD_time'    : [0., 256.],
              #'global_time' : [0., 256.],
              'amplitude'   : [0., 150.],
              #'charge' : [-35e3, 0.],
    }
    histos1d = {}
    nCh = 16
    nbins = 50
    for varname in varnames:
        x1 = varnames[varname][0]
        x2 = varnames[varname][1]
        histos1d[varname] = []
        for ich in range(0, nCh):
            name = 'h_{}_{}'.format(varname, ich)
            title = varname + ';' + varname
            histos1d[varname].append(ROOT.TH1D(name, title, nbins, x1, x2))
    
   
    
    ChNames = ['Channel{}'.format(i) for i in range(0,16)]

    # random event to draw:
    irand = 267

    forms2d = {}
    nx, x1, x2 = int(1024/4), 0., 640
    ny, y1, y2 = int(500/10), 3000., 3700.
    
    amplitudes2d = {}
    nx, x1, x2 = 70, 50., 120.
    for ChName1 in ChNames:
        for ChName2 in ChNames:
            if ChName1 == ChName2:
                continue
            name = ChName2 + '_vs_' + ChName1
            title = name
            amplitudes2d[name] = ROOT.TH2D(name, title, nx, x1, x2, nx, x1, x2, )

    ############
    #   Loop   #
    ############
        

    nReq = nEvts
    # HACKS!
    #nReq = 100
    iDraw = 1000

    """
    canname = 'WaveForms1d_{}'.format(runtag)
    cw = 1000
    ch = 1000
    can = ROOT.TCanvas(canname, canname, 200, 200, cw, ch)
    can.Divide(2,1)
    cans.append(can)
    """
    
    for ievt in range(0, nReq):

        if ievt % iDraw == 0:
            print('{} / {} / {}'.format(ievt, nReq, nEvts))
        # get the next tree in the chain
        ientry = fchain.LoadTree(ievt)
        if ientry < 0:
            print('Breaking!')
            break

        # verify file/tree/chain integrity
        nb = fchain.GetEntry( ievt )
        if nb <= 0:
            print('Sorry, continuing...')
            continue       

        """
        # old by hand:
        hwf0 = fchain.Channel0
        hwf8 = fchain.Channel8
        hmean = hwf0.GetMean()
        hmax = hwf0.GetMaximum()
        hmin = hwf0.GetMinimum()
        #print(hmean, hmax, hmin)
        # plot a random event waveform
        if ievt % 10 == 0:
            SetOpts(hwf0)
            SetOpts(hwf8)
            can.cd(1)
            hwf0.Draw('hist')
            can.cd(2)
            hwf8.Draw('hist')
            #stuff.append(hwf0)
            nbx = hwf0.GetXaxis().GetNbins()
            x1 = hwf0.GetXaxis().GetXmin()
            x2 = hwf0.GetXaxis().GetXmax()
            #print(nbx, x1, x2)
            ROOT.gPad.Update()
            #ROOT.gApplication.Run()
        """

        for varname in varnames:
            for ich in range(0, nCh):
                name = 'h_{}_{}'.format(varname, ich)
                amplitude = GetAmplitude(eval('fchain.Channel{}'.format(ich)))
                #print(amplitude)
                histos1d[varname][ich].Fill(amplitude)
    

    ############
    #   PLOT   #
    ############


    # 1d
    for varname in histos1d:
        canname = '{}1d_{}'.format(varname, runtag)
        cw = 800
        ch = 800
        can = ROOT.TCanvas(canname, canname, 0, 0, cw, ch)
        can.Divide(4,4)
        cans.append(can)
        ican = 0
        for ich in range(0, nCh):
            name = 'h_{}_{}'.format(varname, ich)
            ican = ican + 1
            can.cd(ican)
            ROOT.gPad.SetLogz(1)
            h1 = histos1d[varname][ich]
            SetOpt1d(h1)
            h1.Draw('hist')
        can.Update()
        can.Print(can.GetName() + '.png')
        can.Print(can.GetName() + '.pdf')


  


    
    ROOT.gApplication.Run()
    return

###################################
###################################
###################################

if __name__ == "__main__":
    # execute only if run as a script"
    main(sys.argv)
    
###################################
###################################
###################################

